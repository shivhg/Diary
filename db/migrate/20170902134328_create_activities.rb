class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.string :kind
      t.text :desctiption
      t.integer :satisfactory_scale

      t.timestamps
    end
  end
end
